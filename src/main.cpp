#include <Homie.h>

const int CHECK_INTERVAL_SECONDS = 1;

unsigned long lastCheck = 0;

const int PIN_LETTER_DOOR = D1;
const int PIN_PACKAGE_DOOR = D2;

bool lastLetterDoorState = false;
bool lastPackageDoorState = false;

HomieNode letterDoorNode("letter-door", "Letter door", "door");
HomieNode packageDoorNode("package-door", "Package door", "door");

void loopHandler() {
    if (millis() - lastCheck >= CHECK_INTERVAL_SECONDS * 1000UL || lastCheck == 0) {
        bool isLetterDoorOpened = !digitalRead(PIN_LETTER_DOOR);
        bool isPackageDoorOpened = !digitalRead(PIN_PACKAGE_DOOR);

        if (isLetterDoorOpened != lastLetterDoorState) {
            Homie.getLogger() << "Letter door is " << (isLetterDoorOpened ? "open" : "closed") << endl;
            letterDoorNode.setProperty("open").send(isLetterDoorOpened ? "true" : "false");
        }

        if (isPackageDoorOpened != lastPackageDoorState) {
            Homie.getLogger() << "Package door is " << (isPackageDoorOpened ? "open" : "closed") << endl;
            packageDoorNode.setProperty("open").send(isPackageDoorOpened ? "true" : "false");
        }

        lastLetterDoorState = isLetterDoorOpened;
        lastPackageDoorState = isPackageDoorOpened;

        lastCheck = millis();
    }
}

void setup() {
    Serial.begin(115200);
    Serial << endl << endl;

    pinMode(PIN_LETTER_DOOR, INPUT);
    pinMode(PIN_PACKAGE_DOOR, INPUT);

    Homie_setFirmware("neonox31-mailbox", "1.0.0");
    Homie.setLoopFunction(loopHandler);

    letterDoorNode.advertise("open")
            .setName("Open")
            .setDatatype("boolean");

    packageDoorNode.advertise("open")
            .setName("Open")
            .setDatatype("boolean");

    Homie.setup();
}

void loop() {
    Homie.loop();
}