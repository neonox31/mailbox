# Mailbox

A DIY sensor to detect when 💌 or 📦 are dropped in my 📫

## Hardware

### Parts
|Label|Part Type|Properties|
|--- |--- |--- |
|J1|Screw terminal - 2 pins|broches 2; Taille du trou 1.0mm,0.508mm; boîtier THT; espacement des broches 0.137in (3.5mm)|
|J2|Screw terminal - 2 pins|broches 2; Taille du trou 1.0mm,0.508mm; boîtier THT; espacement des broches 0.137in (3.5mm)|
|R1|10kΩ Resistor|résistance 10kΩ; tolérance ±5%; variante mini_resistor; boîtier THT; bands mini 4; espacement des broches 200 mil|
|R2|10kΩ Resistor|résistance 10kΩ; tolérance ±5%; variante vertical; boîtier THT; bands Vertical; espacement des broches 100 mil (stand-up right)|
|WeMos D1 Mini Protoboard Shield1|WeMos D1 Mini Protoboard Shield|headers no headers; variante Protoboard Shield|
|WeMos D1 Mini1|WeMos D1 Mini|cpu ESP-8266EX; headers female above; variante variant 1|

### Breadboard layout
![Breadboard layout](doc/hardware/mailbox_bb.png)

## Software

[![works with MQTT Homie](https://homieiot.github.io/img/works-with-homie.svg "works with MQTT Homie")](https://homieiot.github.io/)

### MQTT topics
```
home / mailbox / letter-door / open → true
home / mailbox / package-door / open → true
```